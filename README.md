# Fully Customizable Pulley Wheel

Design your own Pulley Wheel!

Comes with optional Bearing Support!

Standard Values are for the "Openbuilds Idle Pulley"!

Notice: If you get strange behavior in the Customizer, your Pulley Wheel is probably to small for the bearing or screw hole

[**Bitbucket repo:**](https://bitbucket.org/simon4web/customizable-pulley-wheel)  

[**Thingiverse:**](https://www.thingiverse.com/thing:3040728)

## Avaliable Bearings:

+ 608
+ 625
+ more to follow
+ Custom

## Version Log:

0.1 - *7.8.2018*:
+ Release Version
